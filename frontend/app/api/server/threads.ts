import queryString from "query-string";
import { fetchDataServer } from "../request";

export async function getThreads(searchParams:any, params:any) {
  console.log(params)
    const urlParams={
      t:searchParams.t,
      page:searchParams.page
    };
    const queryFilter=queryString.stringify(urlParams);
    const threads = await fetchDataServer(`threads/topic/${params?.slug?params.slug[params?.slug.length-1]+'/':''}?${queryFilter}`);
    return threads;
  }
export async function getLastestThreads(){
  const threads= await fetchDataServer(`threads/lastest/`);
  return threads;
}
  