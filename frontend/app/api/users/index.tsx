import { fetchDataServer } from "../request";

export const getProfile=async ()=>{
    const profile = await fetchDataServer('users/profile/')
    return profile;
}