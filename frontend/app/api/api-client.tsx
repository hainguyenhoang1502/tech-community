
export const fetchDataClient=async (url:string)=>{
    const res= await fetch(`${process.env.BASE_URL_CLIENT}/api/${url}`).then(response=>response.json()).catch(error=>console.log(error)); 
    return res;
}