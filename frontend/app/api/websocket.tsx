export const websocketNotification=()=>{
    const ws=new WebSocket(`${process.env.BASE_URL_WEBSOCKET}/notifications`)
    return ws
}