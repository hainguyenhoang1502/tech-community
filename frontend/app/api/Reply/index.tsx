
'use server'

import { postDataServer } from "../request";




export const handleAddReply=async (replyId:string, replyContent:string)=>{
    const newComment={
        reply_id:replyId,
        content:replyContent
      }
  
      const res= await postDataServer('replies/add-reply/', newComment);
      console.log(res);
      return res;
} 