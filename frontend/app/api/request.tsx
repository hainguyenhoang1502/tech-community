'use server'

import { revalidateTag } from "next/cache";
import { cookies } from "next/dist/client/components/headers";


const Token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjkwODg0Mzk5LCJpYXQiOjE2OTA0NTIzOTksImp0aSI6IjFkNGIyNzFkYTQzNDQxNjhiZDUwMTliYTE3NmMwMWMwIiwidXNlcl9pZCI6Mn0.iOf-Jo-NirK6ty3XqRdEbsuhiPFJSkSD_VvCWAFWXtc"
export const fetchDataServer = async (url: string) => {

  const access_token= cookies().get('access_token')
  console.log(access_token?.value)
  const headers = {
    Authorization:`Bearer ${access_token?.value}`
  };

  try {
    const res = await fetch(`${process.env.BASE_URL_SERVER}/${url}`, {
      headers,
      cache:'no-cache',
      next:{
        tags:['replies'],
      }
    });

    if (res.status === 404)
      return { code: res.status, data: null, message: "Not found" };

    if (res.status === 401)
      return {
        code: res.status,
        data: null,
        message: "Phiên đăng nhập đã hết! Vui lòng đăng nhập lại!",
      };

    if (res.status === 200) {
      const data = await res.json();
      return { code: res.status, data: data, message: "Success!" };
    }

    // Handle other status codes if needed
    // For example, you might add a generic error message for unexpected status codes.

    return { code: res.status, data: null, message: "Unexpected status code" };
  } catch (error) {
    // Handle any network or fetch-related errors here
    return { code: 500, data: null, message: "Network error" };
  }
};

export const postDataServer = async (url: string, data: any) => {

  const access_token= cookies().get('access_token')

  const headers = {

    Authorization:`Bearer ${access_token?.value}`,
    "Content-Type": "application/json", // Set the Content-Type to JSON for a POST request
  };

  try {
    const res = await fetch(`${process.env.BASE_URL_SERVER}/${url}`, {
      method: "POST",
      headers,
      body: JSON.stringify(data), // Convert data to JSON string before sending
    });

    if (res.status === 401)
      return {
        code: res.status,
        data: null,
        message: "Phiên đăng nhập đã hết! Vui lòng đăng nhập lại!",
      };
    
    if(res.status===400){
      revalidateTag('replies')
      return {
        code: res.status,
        data:null,
        message: "Reply has been deleted!"
      }
      
    }

    if (res.ok) {
      const responseData = await res.json(); 
      revalidateTag('replies')
      return { code: res.status, data: responseData, message: "Success!" };
    }

    // Handle other status codes if needed
    // For example, you might add specific error messages for other status codes.

    return { code: res.status, data: null, message: "Unexpected status code" };
  } catch (error) {
    // Handle any network or fetch-related errors here
    return { code: 500, data: null, message: "Network error" };
  }
};
