import { redirect } from "next/navigation";
import { fetchDataServer } from "./api/request";
import CategoryCard from "./components/Category/CategoryCard";




type HomeProps = {
  params?: {
    slug?: string;
  };
  searchParams?: {
    t?: string;
    page?: number;
  };
}
const getListFilters = async (cate: string | undefined) => {
  const categories = await fetchDataServer(`categories/${cate ? cate : ''}`);
  console.log(categories);
  return categories;
}

export default async function Home({ params, searchParams }: HomeProps) {
  const listCate = await getListFilters(params?.slug)


  if(listCate.code===401){
    redirect('/auth/login')
  }
  
  return (
    <div className=" py-3 rounded-lg  text-black">
      <div className="my-[100px]">
        <h1 className="text-2xl font-bold mb-[30px]">Categories</h1>
        <ul className='grid grid-cols-3 gap-4'>
          {listCate.data?.children?.map((item: any, index: number) => {
            return (
              <li key={index} className=''>
                <CategoryCard title={item.name} description={item.description} slug={item.slug} />
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}
