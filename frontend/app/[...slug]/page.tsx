import React from "react";
import { getThreads } from "../api/server/threads";
import FilterForum from "../components/Common/Filter";
import ThreadList from "../components/Threads/ThreadList";
import { fetchDataServer } from "../api/request";
import CategoryCard from "../components/Category/CategoryCard";
import { notFound, redirect } from "next/navigation";
import ThreadProvider from "../context/Thread";

type HomeProps = {
  params?: {
    slug?: string[];
  };
  searchParams?: {
    t?: string;
    page?: number;
  };
};
const getListFilters = async (cate: string | undefined) => {
  const categories = await fetchDataServer(`categories/${cate ? cate : ""}`);
  return categories;
};
export default async function Home({ params, searchParams }: HomeProps) {
  const listCate = await getListFilters(
    params?.slug ? params.slug[params?.slug.length - 1] : ""
  );
  const threadList = await getThreads(searchParams, params);
  if (!threadList.data && !listCate.data) {
    notFound();
  }
  if (threadList.code === 401 || listCate.code === 401) {
    redirect("/auth/login");
  }
  if (listCate.data?.children && listCate.data.children?.length > 0) {
    return (
      <div className=" h-screen py-3 rounded-lg text-black">
        <div className="my-[100px]">
          <h1 className="text-2xl font-bold mb-[30px]">Categories</h1>
          <ul className="grid grid-cols-3 gap-4">
            {listCate.data?.children?.map((item: any, index: number) => {
              return (
                <li key={index} className="w-full">
                  <CategoryCard
                    title={item.name}
                    description={item.description}
                    slug={item.slug}
                  />
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
  if (listCate.data?.topics && listCate.data.topics?.length > 0) {
    return (
      <div className="h-screen py-3 rounded-lg text-black">
        <div className="my-[100px]">
          <h1 className="text-2xl font-bold mb-[30px]">Topics</h1>
          <ul className="grid grid-cols-3 gap-4">
            {listCate.data?.topics?.map((item: any, index: number) => {
              return (
                <li key={index} className="w-full">
                  <CategoryCard
                    title={item.title}
                    description={item.description}
                    slug={item.slug}
                  />
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }

  if (threadList && threadList.data) {
    return (
      <div className="h-screen py-3 rounded-lg gap-3 text-black">
        <div className="my-[100px] basis-4/5 p-5 rounded-lg bg-white text-black">
          <h1 className="text-3xl font-bold">Tech Community</h1>
          <ThreadProvider>
            <ThreadList
              listThreads={threadList.data}
              topicId={
                params?.slug ? params.slug[params?.slug.length - 1] + "/" : ""
              }
            />
          </ThreadProvider>
        </div>
      </div>
    );
  }
}
