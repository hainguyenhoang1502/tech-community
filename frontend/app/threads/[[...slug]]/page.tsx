import { fetchDataServer } from "../../api/request";
import { notFound, redirect } from "next/navigation";
import { getProfile } from "@/app/api/users";
import ThreadCard from "@/app/components/Threads/ThreadCards";
import RepliesList from "@/app/components/Replies/RepliesList";
import ThreadReply from "@/app/components/Threads/ThreadReply";

export async function getThreads(cate: string) {
  const thread_slug = cate.split(".");
  const thread_id = thread_slug[thread_slug.length - 1];
  const threads = await fetchDataServer(`threads/${thread_id}/`);
  console.log("threads: ", threads);
  return threads;
}
export async function getReplies(cate: string) {
  const thread_slug = cate.split(".");
  const thread_id = thread_slug[thread_slug.length - 1];
  const replies = await fetchDataServer(`replies/${thread_id}`);
  console.log("replies: ", replies);
  return replies;
}

export default async function Page({ params }: { params: { slug: string } }) {
  const threads = await getThreads(params.slug.toString());
  const replies = await getReplies(params.slug.toString());
  const profile = await getProfile();

  if (threads.code === 401 || replies.code === 401) {
    redirect("/auth/login");
  }
  if (threads.code === 404) {
    notFound();
  }

  return (
    <div className="py-3 rounded-lg  text-black">
      <div className="my-[100px] bg-white p-10 rounded-lg text-black">
        <ThreadCard
          title={threads.data[0].title}
          id={threads.data[0].id}
          user={threads.data[0].user}
          content={threads.data[0].content}
          created_at={threads.data[0].created_at}
          thread={threads.data[0].thread}
        />
        <ThreadReply threadId={threads.data[0].id} />
          {replies?.data && replies?.data?.length > 0 && (
            <RepliesList repliesData={replies?.data} index={0} />
          )} 
      </div>
    </div>
  );
}
