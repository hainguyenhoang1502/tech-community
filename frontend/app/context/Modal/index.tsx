"use client";

import React, { createContext } from "react";
import ReactDOM from "react-dom";

type Props = {
  children: React.ReactNode;
};

const ModalsContext = createContext<any>({});

export const useModals = () => React.useContext(ModalsContext);

export function ModalsProvider({ children }: Props) {
  const [modalName, setModalName] = React.useState<string>("");
  const [modals, setModals] = React.useState<any>({});

  const registerModal = (modalName: string, Component: React.ReactNode) => {
    if (!modals[modalName]) {
      setModals({
        ...modals,
        [modalName]: Component,
      });
    }
  };

  const openModal = (modalName: string) => {
    setModalName(modalName);
  };

  const closeModal = () => {
    setModalName("");
  };
  const modalRoot = document.getElementById("modal");
  const modalPortal = modalRoot ? ReactDOM.createPortal(modals[modalName], modalRoot) : null;
  return (
    <ModalsContext.Provider value={{ registerModal, closeModal, openModal }}>
      {modalName && <div className="fixed top-0 left-0 bottom-0 right-0 z-100 bg-slate-500 bg-opacity-50">{modalPortal}</div>}
      {children}
    </ModalsContext.Provider>
  );
}

export default ModalsProvider;
