"use client"

import { postDataServer } from "@/app/api/request";
import { getTextFromEditorState } from "@/app/utils";
import { Editor, EditorState } from "draft-js";
import { useParams } from "next/navigation";
import React from "react";
import { toast } from "react-toastify";
import { useModals } from "../Modal";
import ModalCreateThreads from "@/app/components/Modals/ModalCreateThreads";

type Props = {};
export const ThreadCtx = React.createContext<any>({});
export const useThread = ()=>React.useContext(ThreadCtx);
const ThreadProvider = ({ children }: { children: React.ReactNode }) => {
  const [createThread, setCreateThread] = React.useState<{
    title: string;
    content: EditorState;
  }>({
    title: "",
    content: EditorState.createEmpty(),
  });

  const {registerModal, closeModal}=useModals();

  const params = useParams();
  registerModal("test", <ThreadProvider><ModalCreateThreads/></ThreadProvider>);

  const onContentChange = (newContent: EditorState) => {
    setCreateThread({
      ...createThread,
      content: newContent,
    });
  };
  const onTitleChange = (newTitle: string) => {
    setCreateThread({
      ...createThread,
      title: newTitle,
    });
  };

  const handleSubmit = async () => {
    const thread = {
      topic: params?.slug ? params.slug.split('/')[params.slug.split('/').length - 1] : "",
      title: createThread.title,
      content: getTextFromEditorState(createThread.content),
    };

    console.log(thread)

    const res= await postDataServer('threads/create/', thread)
    console.log(res)
    if(res.code===201){
        toast.success('Create thread successfully!')
        closeModal()
    }
    if(res.code===400){
        toast.error(res.message)
    }
  };

  return <ThreadCtx.Provider value={{handleSubmit, onTitleChange, onContentChange, createThread}}>{children}</ThreadCtx.Provider>;
};

export default ThreadProvider;
