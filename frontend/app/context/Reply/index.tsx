"use client"

import { getTextFromEditorState } from "@/app/utils";
import { EditorState } from "draft-js";
import React from "react";
import { useModals } from "../Modal";
import ModalCreateThreads from "@/app/components/Modals/ModalCreateThreads";

type Props = {
  children: React.ReactNode;
};

export const ReplyContext = React.createContext<any>({});

const ReplyCtxProvider = ({ children }: Props) => {
  const [replies, setReplies] = React.useState<any>([]);
  const [replyId, setReplyId] = React.useState("");
  const [threadId, setThreadId] = React.useState("");
  const {registerModal}=useModals()
  const [comment, setComment] = React.useState<EditorState>(
    EditorState.createEmpty()
  );

  

  const handleRefresh = () => {
    setReplyId("");
    setComment(EditorState.createEmpty());
  };
  console.log(getTextFromEditorState(comment));
  return (
    <ReplyContext.Provider
      value={{
        replyId,
        setReplyId,
        comment,
        setComment,
        threadId,
        setThreadId,
        replies,
        setReplies,
        handleRefresh,
      }}
    >
      {children}
    </ReplyContext.Provider>
  );
};

export default ReplyCtxProvider;
