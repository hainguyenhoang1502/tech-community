import Link from 'next/link'

export default function NotFound() {
    return (
        <div className='my-[100px] h-[90vh] flex w-full items-center justify-center'>
            <div>
                <h2 className='text-5xl mb-10 font-bold'>
                    <span className='border-r-2 p-3'>404</span>
                    <span className='p-3'>Not Found</span>
                </h2>
                <p className='mb-10'>Could not find any page match with your request</p>
                 <Link href='/' className='p-3 bg-cyan-500 rounded-lg text-white'>Back to Home Page</Link>
            </div>

        </div>
    )
}