export type ThreadItemType={
    id:number
    title:string
    content:string
    created_at:Date
    published:boolean
    topic:number
    user:UserType
    replies:any[]
}

export type UserType={
    id:number
    email:string
    user_name:string
    last_name:string
    first_name:string
    profile_image:string
    groups:any[]
    user_permissions:any[]
}