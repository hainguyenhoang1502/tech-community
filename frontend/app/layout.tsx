import "./globals.css";
import type { Metadata } from "next";
import { Open_Sans } from "next/font/google";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ModalContainer, { ModalsProvider } from "./context/Modal";

const lato = Open_Sans({
  weight: ["300", "400", "700"],
  subsets: ["latin", "latin-ext"],
});

export const metadata: Metadata = {
  title: "Car Community",
  description: "Community for car lovers",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`${lato.className} bg-slate-100`}>
      <ModalsProvider>

        <main>
          <Navbar />

            <section className="px-5 w-full mx-auto flex-row max-w-[1440px]">
              {children}
            </section>
          {/* <Footer /> */}
        </main>
        <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="light"
            />
        <div id="modal"></div>
        </ModalsProvider>

      </body>
    </html>
  );
}
