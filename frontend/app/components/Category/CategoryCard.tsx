"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";

type Props = {
  title: string;
  description: string;
  slug: string;
};

const CategoryCard = ({ title, description, slug }: Props) => {
  const pathname = usePathname();
  return (
    <Link href={pathname !== "/" ? `${pathname + "/" + slug}` : slug}>
      <div
        className="p-6 w-full bg-white border
         border-gray-200 rounded-lg shadow dark:bg-gray-800
          dark:border-gray-700
          cursor:pointer
          hover:drop-shadow-xl
          "
      >
        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
          {title}
        </h5>
        <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
          {description}
        </p>
        <Link
          href={pathname !== "/" ? `${pathname + "/" + slug}` : slug}
          className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-black rounded-lg dark:focus:ring-blue-800"
        >
          Read more
          <svg
            className="w-3.5 h-3.5 ml-2"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 10"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M1 5h12m0 0L9 1m4 4L9 9"
            />
          </svg>
        </Link>
      </div>
    </Link>
  );
};

export default CategoryCard;
