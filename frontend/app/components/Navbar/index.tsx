import Link from "next/link";
import React from "react";
import SearchInput from "../Common/SearchInput";
import Button from "../Common/Button";
import { fetchDataServer } from "@/app/api/request";
import { getProfile } from "@/app/api/users";
import Notifications from "../Notifications";

type Props = {};

const Navbar = async () => {
  const profile = await getProfile();
  console.log(profile);

  if (!profile.data) {
    return (
      <nav className="text-[1rem] w-full h-[100px] fixed top-0 z-10 p-5 bg-white px-5">
        <div className="flex max-w-[1440px] mx-auto justify-between items-center flex-row gap-[20px]">
          <Link href="/">
            <h1 className="text-4xl tracking-wider text-black font-bold">
              TechCOM
            </h1>
          </Link>
          <div className="md:grow hidden md:rounded-full md:max-h-full md:block">
            <SearchInput />
          </div>
          <div className="flex text-black flex-row items-center gap-[10px]">
            <Link
              href="/auth/login"
              className="py-2 px-5 bg-black text-white rounded-lg"
            >
              Login
            </Link>
            <Link href="/auth/register" className="p-3 rounded-lg">
              Sign up
            </Link>
          </div>
        </div>
      </nav>
    );
  }

  return (
    <nav className="text-[1rem] w-full h-[100px] fixed top-0 z-10 p-5 bg-white px-5">
      <div className="flex max-w-[1440px] mx-auto justify-between items-center flex-row gap-[20px]">
        <Link href="/">
          <h1 className="text-4xl tracking-wider text-black font-bold">
            TechCOM
          </h1>
        </Link>
        <div className="md:grow hidden md:rounded-full md:max-h-full md:block">
          <SearchInput />
        </div>
        <div className="flex text-black flex-row items-center gap-[30px]">
          <Button icon={"solar:letter-linear"} />
          <Notifications />
          <img
            className="w-10 h-10 rounded-full cursor-pointer"
            src={`http://127.0.0.1:8000${profile?.data?.profile_image}`}
            alt="Rounded avatar"
          ></img>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
