import { useModals } from "@/app/context/Modal";
import { useThread } from "@/app/context/Thread";
import React from "react";
import TextEditor from "../Common/TextEditor";
import Button from "../Common/Button";

type Props = {};

const ModalCreateThreads = (props: Props) => {
  const { closeModal } = useModals();
  const { handleSubmit, onTitleChange, onContentChange, createThread } =
    useThread();

  return (
    <div className="absolute top-[50%] left-[50%] -translate-x-[50%] -translate-y-[50%] w-full max-w-2xl max-h-full">
      <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
        <div className="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
          <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
            Create Threads
          </h3>
          <button
            type="button"
            onClick={() => closeModal()}
            className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
            data-modal-hide="defaultModal"
          >
            <svg
              className="w-3 h-3"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 14 14"
            >
              <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
              />
            </svg>
            <span className="sr-only">Close modal</span>
          </button>
        </div>
        <div className="p-6">
          <label
            htmlFor="first_name"
            className="block text-sm font-bold mb-2 text-gray-900 dark:text-white"
          >
            Thread Title
          </label>
          <input
            type="text"
            id="first_name"
            className="bg-gray-50 mb-10 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="Title"
            required
            value={createThread.title}
            onChange={(e) => onTitleChange(e.target.value)}
          />
          <div className="w-full">
            <label
              htmlFor="first_name"
              className="block text-sm font-bold mb-2 text-gray-900 dark:text-white"
            >
              Thread Content
            </label>
            <TextEditor
              editorState={createThread.content}
              onChange={onContentChange}
            />
          </div>
        </div>
        <div className="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
          <Button
            onClick={handleSubmit}
            text="Create"
            className="text-white bg-black  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          />
          <Button
            onClick={()=>closeModal()}
            text="Cancel"
            className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-black text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
          />
        </div>
      </div>
    </div>
  );
};

export default ModalCreateThreads;
