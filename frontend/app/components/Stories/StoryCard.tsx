import Image from 'next/image'
import React from 'react'

type Props = {}

const StoryCard = (props: Props) => {
  return (
    <div className='flex items-center bg-slate-900 overflow-hidden justify-center mx-2 rounded-2xl '>
        <img src="avatar.jpg" alt="avatar" className='h-full object-cover max-w-none' />
    </div>
  )
}

export default StoryCard