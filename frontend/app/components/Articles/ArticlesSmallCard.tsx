import Link from 'next/link'
import React from 'react'

type Props = {}

const ArticlesSmallCard = (props: Props) => {
  return (
    <div className="max-w-full lg:flex-wrap lg:flex lg:flex-row bg-white rounded-lg">
      <Link href="#" className="lg:flex-2 lg:p-2">
        <img className="rounded-lg drop-shadow-xl object-cover w-full " src="avatar.jpg" alt="" />
      </Link>
      <div className="p-5 md:flex-1 flex flex-col justify-between">
        <Link href="#">
          <h1 className="mb-2 text-3xl font-bold tracking-tight text-black">
          'John Wick 4' thu hơn 137 triệu USD tuần đầu
          </h1>
        </Link>
        <p className="mb-3 font-normal text-gray-700 text-ellipsis max-h-12 overflow-hidden dark:text-gray-400 ">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab tempora nulla voluptatem odio similique veritatis fuga optio suscipit ipsa illo! Id cumque libero nulla facilis voluptatum facere? Unde, fuga commodi?
        </p>
        <Link
          href="#"
          className="inline-flex max-w-max items-center px-3 py-2 text-sm font-medium text-center text-white cursor-pointer hover:bg-blue-600 bg-blue-400 rounded-full focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Read more
          <svg
            className="w-3.5 h-3.5 ml-2"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 10"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M1 5h12m0 0L9 1m4 4L9 9"
            />
          </svg>
        </Link>
      </div>
    </div>
  )
}

export default ArticlesSmallCard