import React from 'react'
import ArticlesSmallCard from './ArticlesSmallCard'

type Props = {}

const LastestArticles = (props: Props) => {
  return (
    <div className='grid lg:grid-cols-4 md:grid-cols-2 grid-cols-1 gap-3'>
        {
            Array.from({length:4}).map(()=>{
                return(
                    <ArticlesSmallCard/>
                )
            })
        }
    </div>
  )
}

export default LastestArticles