import React from "react";

type Props = {
  children: React.ReactNode;
  classes?:string
};

const Grid: React.FC<Props> = ({ children, classes }) => {
  return <div
  className={"flex flex-row justify-between gap-[20px] items-center " + classes}
  >{children}</div>;
};

export default Grid;
