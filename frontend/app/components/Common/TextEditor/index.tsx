import { convertToRaw, EditorState, ContentState, convertFromHTML } from "draft-js";
import React, { useState, useEffect } from "react";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";

type Props = {
  editorState: EditorState;
  onChange: (newText: EditorState) => void;
  height?:number;
};

const TextEditor = ({ editorState, onChange, height }: Props) => {

  const editorRef=React.useRef<Editor>(null);
  // Update the "text" prop whenever the EditorState changes
  const onEditorStateChange = (newEditorState: EditorState) => {
    onChange(newEditorState)
  };

  useEffect(()=>{
    editorRef?.current?.focusEditor();
    const updatedEditorState = EditorState.moveFocusToEnd(editorState);
    // Call the onChange function to update the EditorState
    onEditorStateChange(updatedEditorState);
  },[])

  return (
    <>
      <div className="border-2 border-slate-300 rounded-xl p-2">
        <Editor
          ref={editorRef}
          editorState={editorState}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          onEditorStateChange={onEditorStateChange}
          
          editorStyle={{
            height: height?height:200, // Set the desired height
          }}
          toolbarStyle={{
            marginBottom: 0,
            border: "none",
          }}
          toolbar={{
            options: [
              "inline",
              "fontSize",
              "list",
              "textAlign",
              "link",
              "image",
              "history",
            ], // List of available toolbar options
            inline: {
              options: ["bold", "italic", "underline"],
            },
            fontSize: {
              options: [10, 12, 14, 16, 18, 24, 30, 36], // Available font size options
            },
            list: {
              inDropdown: true, // Display the list options in a dropdown
              options: ["unordered", "ordered", "indent", "outdent"],
            },
            textAlign: {
              inDropdown: true, // Display the text alignment options in a dropdown
              options: ["left", "center", "right", "justify"],
            },
            link: {
              defaultTargetOption: "_self", // Default target option for links
            },
          }}
        />
      </div>
    </>
  );
};

export default TextEditor;
