"use client";

import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React from 'react';

type Props = {
    page: {
        has_next: boolean;
        has_previous: boolean;
        next_page_number: number;
        previous_page_number: number;
        number: number;
        total_pages: number;
        count: number;
    };
};

const Pagination = ({ page }: Props) => {
    const router = useRouter()
    const pathName=usePathname()
    const searchParams = useSearchParams()!
    const params=new URLSearchParams(searchParams.toString())
    const page_index=params.get("page")? params.get("page"):'1'

    const createQueryString = React.useCallback(
        (name: string, value: string) => {
          params.set(name, value)
     
          return params.toString()
        },
        [searchParams]
      )

    return (
        <nav aria-label="Page navigation example">
            <ul className="flex items-center -space-x-px h-8 text-sm">
                <li className={page?.has_previous ? 'pointer-events-auto' : 'pointer-events-none'}>
                    <button onClick={() => {router.push(pathName + '?' + createQueryString('page', page?.previous_page_number.toString()))}}
                        className={`flex items-center justify-center px-3 h-8 ml-0 leading-tight ${page?.has_previous ? 'text-gray-500' : 'text-gray-200'} bg-white border border-gray-300 rounded-l-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white`}
                    >
                        <span className="sr-only">Previous</span>
                        <svg className="w-2.5 h-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 1 1 5l4 4" />
                        </svg>
                    </button>
                </li>
                {Array.from({ length: page?.total_pages }).map((item, index) => (
                    <li key={index}>
                        <button onClick={() => {router.push(pathName + '?' + createQueryString('page', (index+1).toString()))}}
                            className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 ${page_index&&parseInt(page_index)===index+1? 'bg-black':'bg-white'} border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white`}
                        >
                            {index + 1}
                        </button>
                    </li>
                ))}
                <li className={page?.has_next ? 'pointer-events-auto' : 'pointer-events-none'}>
                    <button
                        onClick={() => {router.push(pathName + '?' + createQueryString('page', page?.next_page_number.toString()))}}
                        className={`flex items-center justify-center px-3 h-8 ml-0 leading-tight ${page?.has_next ? 'text-black' : 'text-gray-200'} bg-white border border-gray-300 rounded-r-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white`}
                    >
                        <span className="sr-only">Next</span>
                        <svg className="w-2.5 h-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 9 4-4-4-4" />
                        </svg>
                    </button>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;
