"use client";

import { Icon } from '@iconify/react'
import React from 'react'

type Props = {
    icon?:string
    text?:string
    className?:string
    onClick?:() => void
}

const Button = ({icon, text, className, onClick }: Props) => {
  return (
    <button type='submit' onClick={onClick} className={`flex flex-row gap-[10px] ${className?className:''}`}>
        {icon&&<Icon icon={icon} style={{ fontSize: '24px' }}/>}
        {text&&<p>{text}</p>}
    </button>
  )
}

export default Button