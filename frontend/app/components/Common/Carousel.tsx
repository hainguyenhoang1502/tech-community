"use client";

import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import StoryCard from "../Stories/StoryCard";

type Props = {};

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 10,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 7,
  },
  tablet: {
    breakpoint: { max: 1024, min: 567 },
    items: 4,
  },
  mobile: {
    breakpoint: { max: 567, min: 0 },
    items: 3,
  },
};

const CarouselField = (props: Props) => {
  return (
    <Carousel
      responsive={responsive}
      swipeable={true}
      itemClass="carousel-item-padding-40-px"
    >
      {Array.from({length:15}).map(()=>{
        return (
            <div>
                <StoryCard/>
            </div>
        )
      })}
    </Carousel>
  );
};

export default CarouselField;
