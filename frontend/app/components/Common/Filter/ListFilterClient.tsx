"use client";

import { Icon } from '@iconify/react';
import Link from 'next/link';
import { useSearchParams, usePathname, useRouter } from 'next/navigation';
import React from 'react'

type Props = {
    filters: any
}
const color = [
    { color: "text-orange-500" },
    { color: "text-amber-500" },
    { color: "text-green-500" },
    { color: "text-sky-500" },
    { color: "text-indigo-500" },
    { color: "text-pink-500" },
    { color: "text-rose-500" },
    { color: "text-red-500" },
    { color: "text-lime-500" },
]
const ListFilterClient = ({ filters }: Props) => {
    const [dropdown, setDropdown] = React.useState<string>("");
    const searchParams = useSearchParams()
    const pathname = usePathname()
    const searchQuery = searchParams ? searchParams.get("t") : null;
    const endcodeQuery = encodeURI(searchQuery || "")
    const router = useRouter()

    const toggleDropdown = (name: string) => {
        if (name === dropdown) {
            setDropdown('')
        }
        else {
            setDropdown(name)
        }
    }

    return (
        <ul className=' overflow-y-auto'>
            {filters?.children?.map((item: any, index: number) => {
                return (
                    <li key={index} className=''>
                        <div className="flex items-center cursor-pointer" onClick={() => toggleDropdown(item.name)} >
                            <span className={color[index].color}>
                                <Icon icon={(dropdown === item.name || item.topics.find((subitem: any) => subitem.id == endcodeQuery)) ? "ph:circle-fill" : "ph:circle-bold"} />
                            </span>
                            <h1 className="text-sm font-bold p-3">
                                {item.name}
                            </h1>
                        </div>
                        {item.topics ? <ul className={`ml-10 ease-in duration-300 overflow-hidden ${dropdown === item.name ? 'opacity-100 h-auto' : 'h-0 opacity-0'}`}>
                            {item.topics.map((subitem: any, subindex: number) => {
                                return (
                                    <li>
                                        <Link href={`${subitem.id == endcodeQuery ? pathname : `?t=${subitem.id}`}`}>
                                            <h3 key={subindex} className={`text-sm font-bold ${subitem.id == endcodeQuery ? color[index].color : 'text-slate-300'} cursor-pointer p-3`}>
                                                {subitem.title}
                                            </h3>
                                        </Link>
                                    </li>
                                );
                            })}
                        </ul> : <span className="font-light">No records found</span>}
                    </li>
                );
            })}
        </ul>
    )
}

export default ListFilterClient