import { fetchDataServer } from '@/app/api/request';
import Link from 'next/link';
import React from 'react'
import ListFilterClient from './ListFilterClient';

type Props = {
    subcategory:any
}

const getListFilters = async (cate:any) => {
    const categories = await fetchDataServer(`categories/${cate?cate:''}`);
    console.log(categories);
    return categories;
}

const FilterForum = async ({subcategory}: Props) => {
    const filters = await getListFilters(subcategory);

    return (
        <section className='flex h-[80vh] flex-col fixed overflow-y-auto max-w-[250px] mt-[100px] bg-white p-5 rounded-lg'>
            <h1 className='text-sm font-semibold mb-3 uppercase text-gray-400'>Filter forums</h1>
            <ListFilterClient filters={filters}/>
        </section>
    )
}

export default FilterForum