import React from 'react'
import LastestUserItem from './LastestUserItem'

type Props = {}

const LastestUser = (props: Props) => {
  return (
    <div className='flex flex-col bg-white lg:p-3 p-2 rounded-lg'>
        <h3 className='text-xl text-black font-bold'>Online Users</h3>
        <ul>
            {Array.from({length:3}).map(()=>(<LastestUserItem profile_image={''} post_title={''} reply_user={''} reply_time={new Date()} topic={''}/>))}
        </ul>
    </div>
  )
}

export default LastestUser