import React from 'react'
import LastestPostItem from './LastestPostItem'

type Props = {}

const LastestPost = (props: Props) => {
  return (
    <div className='flex flex-col bg-white lg:p-3 p-2 rounded-lg'>
        <h3 className='text-xl text-black font-bold'>Lastest Posts</h3>
        <ul>
            {Array.from({length:5}).map(()=>(<LastestPostItem profile_image={''} post_title={''} reply_user={''} reply_time={new Date()} topic={''}/>))}
        </ul>
    </div>
  )
}

export default LastestPost