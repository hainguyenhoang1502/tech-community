import React from 'react'
import { LastestThreads } from './LastestThreads'
import LastestPost from './LastestPost'
import LastestUser from './LastestUser'

type Props = {}

const SideBar = () => {
  return (
    <div className='flex flex-col gap-3'>
        <LastestThreads/>
        <LastestPost/>
        <LastestUser/>
    </div>
  )
}

export default SideBar