import moment from "moment";
import Link from "next/link";
import React from "react";

type Props = {
  profile_image: string;
  post_title: string;
  reply_user: string;
  reply_time: Date;
  topic: string;
};

const LastestPostItem = (props: Props) => {
  return (
    <li className="flex items-start my-3 gap-2 bg-white rounded-lg flex-row">
      <Link href="#" className="lg:flex-2 pt-1">
        <img
          className="w-10 h-10 rounded-full cursor-pointer"
          src="avatar.jpg"
          alt="Rounded avatar"
        ></img>
      </Link>
      <div className="flex flex-col">
        <Link href="#">
          <h1 className="text-md hover:text-[red]">Post Title here</h1>
        </Link>
        <span className="">Post content here</span>
        <span className="text-[gray] text-sm">{moment().calendar()}</span>
      </div>
    </li>
  );
};

export default LastestPostItem;
