import React, { Children } from 'react'

type Props = {
    children: React.ReactNode
}

const Container:React.FC<Props>=({children}) => {
  return (
    <div className='max-w-[1920px] mx-auto bg-black-color px-[20px]'>
        {children}
    </div>
  )
}

export default Container