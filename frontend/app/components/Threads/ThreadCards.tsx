import moment from "moment";
import React from "react";

type Props = {
  title: string;
  id: number;
  user: {
    id: number;
    user_name: string;
    last_name: string;
    first_name: string;
    profile_image: string;
    groups: string[];
  };
  content: string;
  created_at: Date;
  thread: number;
};

const ThreadCard = ({
  title,
  id,
  user,
  content,
  created_at,
  thread,
}: Props) => {
  return (
    <div className="flex my-3 rounded-lg bg-white flex-col gap-5 w-full">

      {title && <h2 className="text-4xl">{title}</h2>}

      <span className="text-sm font-light text-slate-500 border-b">
        Create at {moment(created_at).calendar()}
      </span>

      <div className="flex flex-row gap-5">
        <img
          src={`http://127.0.0.1:8000${user?.profile_image}`}
          className="object-cover rounded-full h-[30px] w-[30px]"
          alt=""
        />
        <div className="flex flex-col">
          <h3 className="text-md font-bold">{user?.user_name}</h3>
          <span className="text-sm font-light">{user?.groups[0]}</span>
        </div>
      </div>

      <div className="absolute h-5 w-5 bg-white -right-[6%] top-10 rotate-45"></div>

      <div
        className="block_post"
        dangerouslySetInnerHTML={{ __html: content }}
      ></div>
    </div>
  );
};

export default ThreadCard;
