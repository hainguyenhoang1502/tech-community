"use client";

import React from "react";
import ReplyForm from "../Replies/ReplyForm";
import ReplyCtxProvider, { ReplyContext } from "@/app/context/Reply";
import { getTextFromEditorState } from "@/app/utils";
import { postDataServer } from "@/app/api/request";
import { EditorState } from "draft-js";

type Props = {
  threadId: string;
};

const ThreadReply = ({ threadId }: Props) => {
  return (
    <ReplyCtxProvider>
      <ThreadReplyForm threadId={threadId} />
    </ReplyCtxProvider>
  );
};

const ThreadReplyForm = ({ threadId }: Props) => {
  const { replyId, comment, setComment, comments, setComments, handleRefresh } =
    React.useContext(ReplyContext);
  const [openComment, setOpenComment] = React.useState(false);
  const handleAddComment = async () => {
    const content = getTextFromEditorState(comment);
    const newComment = {
      thread_id: threadId,
      content: content,
    };  
    console.log(comment.getCurrentContent().hasText())

    if (comment.getCurrentContent().hasText()) {
      const res = await postDataServer("replies/add-reply/", newComment);
      console.log(res);
      if (res.code === 201) {
        handleRefresh();
      }
    }
  };
  return (
    <ReplyForm
      handleSubmit={handleAddComment}
      value={comment}
      onChange={setComment}
    />
  );
};

export default ThreadReply;
