import moment from "moment";
import Link from "next/link";
import React from "react";

type Props = {
    image:string
    created_at:Date
    user_name:string
    title:string
};

const ThreadItem = (props: Props) => {
  return (
    <div className="flex my-4 items-center gap-5 bg-white p-5 rounded-lg flex-row">
      <Link href="#" className="lg:flex-2 lg:p-2">
        <img
          className="w-10 h-10 rounded-full cursor-pointer"
          src={`http://127.0.0.1:8000${props.image}`}
          alt="Rounded avatar"
        ></img>
      </Link>
      <div className="flex flex-col">
        <Link href="#"><h1 className="text-2xl hover:text-[red]">{props.title}</h1></Link>
        <div className="flex gap-3 text-[gray] flex-row">
            <p>{props.user_name}</p>
            <p>{moment(props.created_at).calendar()}</p>
        </div>
      </div>

    </div>
  );
};

export default ThreadItem;
