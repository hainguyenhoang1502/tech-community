"use client";

import Link from "next/link";
import React from "react";
import Pagination from "../Common/Paginations";
import moment from "moment";
import Button from "../Common/Button";
import { useModals } from "@/app/context/Modal";
import ModalCreateThreads from "../Modals/ModalCreateThreads";

type Props = {
  listThreads: {
    data: any[];
    page: any;
  };
  topicId:string;
};
const ThreadList = ({ listThreads, topicId }: Props) => {
  const { openModal } = useModals();
  console.log(topicId)
  return (
    <section className="mb-[80px] py-3 rounded-lg bg-white text-black">
      <div className="flex my-5 justify-between flex-row w-full">
        <h1 className="text-lg font-bold">Latest Threads</h1>
        <Button
          onClick={() => openModal("test")}
          icon="streamline:interface-edit-write-2-change-document-edit-modify-paper-pencil-write-writing"
          text="Post Threads"
          className="border p-3 rounded-lg border-black"
        />
      </div>

      {listThreads && listThreads.data && listThreads?.data?.length ? (
        listThreads?.data?.map((item: any, index: number) => {
          return (
            <Link href={`/threads/${item.slug}/`} className="flex flex-row items-center py-3 gap-5 border-b">
              <img
                src={`http://127.0.0.1:8000${item?.user?.profile_image}`}
                className="h-10 w-10 rounded-full object-cover"
                alt=""
              />
              <span className="flex flex-col justify-center">
                <span className="text-xs ">
                  <strong>{item?.user?.user_name} </strong>created at:{" "}
                  {moment(item?.created_at).calendar()}
                </span>
                <h1 className="text-xl cursor-pointer py-3">
                  <span>{item.title}</span>
                </h1>
              </span>
            </Link>
          );
        })
      ) : (
        <span className="font-light">No records found</span>
      )}
      <div className="flex my-5 justify-end flex-row w-full">
        <Pagination page={listThreads?.page} />
      </div>
    </section>
  );
};

export default ThreadList;
