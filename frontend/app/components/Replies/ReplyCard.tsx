"use client";

import { ReplyContext } from "@/app/context/Reply";
import moment from "moment";
import React from "react";
import Button from "../Common/Button";
import {
  createEditorStateFromString,
  getTextFromEditorState,
} from "@/app/utils";
import TextEditor from "../Common/TextEditor";
import { EditorState } from "draft-js";
import ReplyForm from "./ReplyForm";
import { postDataServer } from "@/app/api/request";
import { comment } from "postcss";
import { revalidateTag } from "next/cache";
import { toast } from "react-toastify";
import useWebSocket from "@/app/hooks";
import { getProfile } from "@/app/api/users";
import useProfile from "@/app/hooks/useProfile";
import { useParams } from "next/navigation";

type Props = {
  id: number;
  user: {
    id: number;
    user_name: string;
    last_name: string;
    first_name: string;
    profile_image: string;
    groups: string[];
  };
  content: string;
  created_at: Date;
  thread: number;
  children?: any;
  hasChild?: boolean;
};

const ReplyCard = ({
  id,
  user,
  content,
  created_at,
  thread,
  children,
  hasChild,
}: Props) => {
  const {
    replyId,
    setReplyId,
    setComment,
    comment,
    comments,
    setComments,
    handleRefresh,
  } = React.useContext(ReplyContext);
  const {sendMessage, setMessage}=useWebSocket('notifications');
  const {profile}= useProfile();
  const params = useParams()

  const handleAddReply = async () => {
    const content = getTextFromEditorState(comment);
    const threadId= params.slug.split('.')[1]
    console.log(params)
    const newComment = {
      reply_id: replyId,
      content: content,
    };


    const res = await postDataServer("replies/add-reply/", newComment);

    if (res.code === 201) {
      const notification= {
        from: user?.id,
        to: profile?.id, 
        thread:threadId,
        message: `${profile?.user_name} has replied to you`
      }
      sendMessage(notification)
      handleRefresh();
    }
    if (res.code === 400) {
      toast.error("Reply has been deleted!");
    }
  };

  return (
    <div className="flex my-3 relative flex-row  w-full gap-5 ">
      <div className="flex flex-col basis-1/7  rounded-lg gap-1 items-start justify-start">
        <img
          src={`http://127.0.0.1:8000${user?.profile_image}`}
          className="object-cover rounded-full h-[50px] w-[50px] ml-3"
          alt="avartar"
        />
      </div>
      <div className="flex flex-col basis-6/7  w-full">
        <div className="flex flex-col gap-2 overflow-scroll  w-full">
          <div className="flex w-full flex-row gap-3">
            <h3 className="text-sm font-bold">{user?.user_name}</h3>
            <span className="text-xs font-light text-amber-500">
              {user?.groups[0]}
            </span>
            <span className="text-xs font-light text-slate-500">
              {moment(created_at).calendar()}
            </span>
          </div>

          <div
            className="text-sm max-w-[1440px]"
            dangerouslySetInnerHTML={{ __html: content }}
          ></div>
          <div className="flex flex-row justify-start text-xs font-bold text-sky-500 hover:text-sky-900 gap-2">
            <Button
              text={"Reply"}
              onClick={() => {
                const comment =
                  `<h3 style="background-color: slate; font-weight:bold;">@${user?.user_name}</h3>`.toString();
                setComment(createEditorStateFromString(comment));
                setReplyId(id);
              }}
            />
          </div>
        </div>
        {replyId === id && (
          <ReplyForm
            handleSubmit={handleAddReply}
            value={comment}
            onChange={setComment}
          />
        )}
      </div>
    </div>
  );
};

export default ReplyCard;
