"use client";

import React from "react";
import ReplyCard from "./ReplyCard";
import ReplyCtxProvider, { ReplyContext } from "@/app/context/Reply";

type Props = {
  repliesData: any;
  index: number;
  parent?:any;
};

const RepliesList = ({ repliesData, index, parent }: Props) => {

  return repliesData?.map((item: any, subindex: number) => (
    <ReplyCtxProvider>
      <ReplyCard
        id={item?.id}
        user={item?.user}
        content={item?.content}
        created_at={item?.created_at}
        thread={item?.thread}
        children={parent? parent:null}
        hasChild={item?.parent}
      />
      {index < 1 ? (
        <div className={`ml-20 border-l border-slate-300`}>
          {item?.children && item?.children?.length > 0 && (
            <RepliesList
              repliesData={item?.children}
              index={index + 1}
            />
          )}
        </div>
      ) : (
        <div>
          <RepliesList
            repliesData={item?.children}
            index={index ? index + 1 : 1}
            parent={item}
          />
        </div>
      )}
    </ReplyCtxProvider>
  ));
};



export default RepliesList;
