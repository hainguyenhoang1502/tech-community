"use client";

import React from "react";
import TextEditor from "../Common/TextEditor";
import Button from "../Common/Button";
import { ReplyContext } from "@/app/context/Reply";
import { postDataServer } from "@/app/api/request";
import { getTextFromEditorState } from "@/app/utils";
import { EditorState } from "draft-js";

type Props = {
  value:EditorState
  onChange:(editorState:EditorState)=>void
  handleSubmit:()=>void
};

const ReplyForm = ({handleSubmit, value, onChange}: Props) => {
  

  return (
    <>
      <form action={handleSubmit} className="flex flex-col items-end gap-3">
        <div className="w-full">
          <TextEditor editorState={value} onChange={onChange} />
        </div>
        <Button
          text="Comment"
          className="border-2 border-black px-5 py-3 rounded-full hover:bg-black hover:text-white"
        />
      </form>
    </>
  );
};

export default ReplyForm;
