'use client';

import useWebSocket from '@/app/hooks'
import React from 'react'
import Button from '../Common/Button';
import useProfile from '@/app/hooks/useProfile';
import Link from 'next/link';

type Props = {}

const Notifications = (props: Props) => {
    const {message, sendMessage, setMessage}=useWebSocket('notifications');
    const {profile}=useProfile();
console.log(message)
  return (
    <div className="relative ">
      <Button icon={"ion:notifications-outline"} />
      {message&&message.length>0&&message.map((item, index)=>{
        return (
            profile?.id===item?.sender&&<div className='absolute p-2 w-[200px] rounded-lg bg-white -left-20 top-[150%] border'>
            <Link href={`threads/${item?.thread?.slug}`}>
                {
                    item?.message
                }
            </Link>
            </div>
        )
      })}
    </div>
  )
}

export default Notifications