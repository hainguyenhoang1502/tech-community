import { EditorState, convertFromHTML, convertToRaw, ContentState } from "draft-js";
import draftToHtml from "draftjs-to-html";

export const createEditorStateFromString = (value: string) => {
  const blocksFromHTML = convertFromHTML(value);

  const contentState = ContentState.createFromBlockArray(
    blocksFromHTML.contentBlocks,
    blocksFromHTML.entityMap
  );
  return EditorState.createWithContent(contentState);
};

export const getTextFromEditorState = (editorState: EditorState) => {
  const htmlContent = draftToHtml(
    convertToRaw(editorState.getCurrentContent())
  );

  return htmlContent;
};

