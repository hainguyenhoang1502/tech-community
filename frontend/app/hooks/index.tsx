'use client';


import { json } from 'node:stream/consumers';
import { useEffect, useState } from 'react';

const useWebSocket = (url:string) => {
  const [message, setMessage] = useState<any[]>([]);
  const [socket, setSocket] = useState<WebSocket|null>(null);

  useEffect(() => {
    const ws = new WebSocket(`${process.env.BASE_URL_WEBSOCKET}/${url}`);

    ws.onopen = () => {
      console.log('WebSocket connection established');
      setSocket(ws);
    };

    ws.onmessage = (event) => {
        console.log('ssssssssss', message, JSON.parse(event.data))
      setMessage([...message, JSON.parse(event.data)]);
    };

    ws.onclose = () => {
      console.log('WebSocket connection closed');
    };

    return () => {
      ws.close();
    };
  }, [url]);

  const sendMessage = (message:any) => {
    if (socket) {
      socket.send(JSON.stringify(message));
    }
  };

  return { message, sendMessage, setMessage };
};

export default useWebSocket;
