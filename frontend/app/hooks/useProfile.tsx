import React from 'react'
import { getProfile } from '../api/users'


const useProfile = () => {
    const [profile, setProfile]=React.useState<any>(null)

    React.useEffect(()=>{
        const getData=async()=>{
            const profile = await getProfile();
            if(profile.data){
                setProfile(profile.data)
            }
        }
        getData()
    }, [])
  return {
    profile
  }
}

export default useProfile