/** @type {import('next').NextConfig} */
const nextConfig = {
    env:{
        BASE_URL_WEBSOCKET:'ws://127.0.0.1:8000',
        BASE_URL_SERVER:'http://127.0.0.1:8000/api',
        BASE_URL_CLIENT:'http://localhost:3000'
    },
    experimental: {
        serverActions: true,
      },
}

module.exports = nextConfig
