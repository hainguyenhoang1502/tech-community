from rest_framework import serializers
from .models import Articles
from replies.serializers import ReplySerializer
from users.serializers import CustomUserSerializer



class ArticleSerializer(serializers.ModelSerializer):
    user= CustomUserSerializer()
    class Meta:
        model= Articles
        fields='__all__'