from django.urls import path
from . import views

urlpatterns = [
    # path('', views.reply_list, name='reply-list-create'),
    path('<int:article_id>', views.reply_list_by_article, name='reply-list-article'),
    path('add-comment/', views.post_comment, name='comment-add')
    
    # Other user-related URLs for the app
]
