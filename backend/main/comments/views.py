from django.shortcuts import render
from .models import Comment
from django.db.models import Q
from .serializers import CommentSerializer
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from articles.models import Articles
from django.shortcuts import get_object_or_404

@api_view(['GET'])
def reply_list_by_article(request, article_id):
    article=get_object_or_404(Comment, id=article_id)
    replies=Comment.objects.filter(article=article).filter(parent=None).order_by("-created_at")

    serializer=CommentSerializer(replies, many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def post_comment(request):
    # Extract the data from the request's POST data
    article_id = request.data.get('article_id')
    comment_id=request.data.get('comment_id')
    user = request.user  # Assuming you are using Django's built-in authentication system


    if comment_id:
        try:
            parent_comment = Comment.objects.get(id=comment_id)
            comment = Comment.objects.create(
                article=parent_comment.article,
                user=user,
                content=request.data['content'],
                parent=parent_comment
            )
        except Comment.DoesNotExist:
            return Response({"error": "Invalid parent reply ID"}, status=400)
        
    # Ensure that the thread_id and content are present in the request
    else:
        if not article_id or 'content' not in request.data:
            return Response({"error": "Invalid request data"}, status=400)

    # Create the reply object
        comment = Comment.objects.create(
            article_id=article_id,
            user=user,
            content=request.data['content'],

        )

    # Serialize the created reply
    serializer = CommentSerializer(comment)

    # Return the serialized data
    return Response(serializer.data, status=201)

@api_view(['GET'])
def reply_list(request):

    queryset = Comment.objects.filter(parent=None).order_by('+created_at')

    search_query= request.query_params.get('search', None)
    thread_query= request.query_params.get('thread', None)

    if search_query:
        queryset= queryset.filter(Q(content__icontains=search_query))
    if thread_query:
        queryset= queryset.filter(Q(thread__exact=thread_query))

    serializer_class= CommentSerializer(queryset, many=True)
    return Response(serializer_class.data)