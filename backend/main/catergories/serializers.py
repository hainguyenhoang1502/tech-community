from rest_framework import serializers
from .models import Category
from topics.serializers import TopicCategoriesSerializer

class CategorySerializer(serializers.ModelSerializer):
    children= serializers.SerializerMethodField()

    def get_children(self, obj):
        children=obj.children.all()
        serializer = self.__class__(children, many=True)
        return serializer.data
    
    topics=TopicCategoriesSerializer(many=True)
    class Meta:
        model= Category
        fields=['id', 'name', 'slug', 'description', 'topics', 'children']

    
    