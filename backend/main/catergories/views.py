from django.shortcuts import render
from rest_framework import generics
from .models import Category
from .serializers import CategorySerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


#API category


#get list
@api_view(['GET'])
def list_category(request):
    categories = Category.objects.filter(name__iexact='Computer')
    serializer = CategorySerializer(categories, many=True)
    return Response(serializer.data[0])


# Get details
@api_view(['GET'])
def category_detail(request, category_slug):
    try:
        category = Category.objects.get(slug=category_slug)
    except Category.DoesNotExist:
        return Response({'detail': 'Category not found.'}, status=404)

    if request.method == 'GET':
        serializer = CategorySerializer(category)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = CategorySerializer(category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)



