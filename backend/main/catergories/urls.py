from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_category, name='cate-list'),
    path('<slug:category_slug>/', views.category_detail, name='cate-detail'),
    # Other user-related URLs for the app
]
