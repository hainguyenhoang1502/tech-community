from django.shortcuts import render
from .models import CustomUser, CustomUserManager
from rest_framework import generics
from .serializers import CustomUserSerializer
from .decorators import validate_request_data
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from django.contrib.auth.models import Group
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated



@api_view(['POST'])
@validate_request_data
def create_user(request):

    email= request.data.get('email')
    password= request.data.get('password')
    user_name= request.data.get('user_name')
    first_name= request.data.get('first_name')
    last_name= request.data.get('last_name')

    
    user = CustomUser(email=email, user_name=user_name, first_name=first_name, last_name=last_name)
    user.set_password(password)
    user.save()

    group=Group.objects.get(name="new user")
    group.user_set.add(user)

    permissions=group.permissions.all()
    user.user_permissions.set(permissions)
    
    serializer = CustomUserSerializer(user)
    return Response(serializer.data, status=201)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_user_profile(request):

    user_serializer=CustomUserSerializer(request.user)

    return Response(user_serializer.data)