from rest_framework import serializers
from .models import CustomUser
from django.contrib.auth.models import Group

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)

class CustomUserSerializer(serializers.ModelSerializer):
    groups=serializers.StringRelatedField(many=True)
    class Meta:
        model=CustomUser
        fields=['id', 'email', 'user_name', 'last_name', 'first_name','profile_image', 'groups', 'user_permissions']


class CustomUserRepliesSerializer(serializers.ModelSerializer):
    groups=serializers.StringRelatedField(many=True)
    class Meta:
        model=CustomUser
        fields=['id', 'user_name', 'last_name', 'first_name', 'profile_image', 'groups']