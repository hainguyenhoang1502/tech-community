from django.urls import path
from . import views

urlpatterns = [
    path('', views.create_user, name='user-create'),
    path('profile/', views.get_user_profile, name='user-profile')
    # Other user-related URLs for the app
]