from django.shortcuts import render
from rest_framework import generics
from .models import Topic
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializers import TopicSerializer

# Create your views here.

@api_view(['GET'])
def list_topic(request):

    topic= Topic.objects.all()
    topic_serializer = TopicSerializer(topic, many=True)

    return Response(topic_serializer.data)


@api_view(['GET'])
def detail_topic(request, topic_slug):

    try:
        topic = Topic.objects.get(slug=topic_slug)
    except Topic.DoesNotExist:
        return Response({'detail': 'topic not found.'}, status=404)

    if request.method == 'GET':
        serializer = TopicSerializer(topic)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = TopicSerializer(topic, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)