from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_topic, name='topic-list-create'),
    path('<slug:topic_slug>/', views.detail_topic, name='topic-detail'),
    # Other user-related URLs for the app
]
