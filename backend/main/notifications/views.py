from django.shortcuts import render
from .models import Comment
from django.db.models import Q
from .serializers import CommentSerializer
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from articles.models import Articles
from django.shortcuts import get_object_or_404

def index(request):

    