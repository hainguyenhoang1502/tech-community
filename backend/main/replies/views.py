from django.shortcuts import render
from .models import Reply
from django.db.models import Q
from .serializers import ReplySerializer
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from threads.models import Thread
from django.shortcuts import get_object_or_404

@api_view(['GET'])
def reply_list_by_thread(request, thread_id):
    thread=get_object_or_404(Thread, id=thread_id)
    print(f"thread: {thread}")
    replies=Reply.objects.filter(thread=thread).filter(parent=None).order_by("-created_at")

    serializer=ReplySerializer(replies, many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def post_reply(request):
    # Extract the data from the request's POST data
    thread_id = request.data.get('thread_id')
    reply_id=request.data.get('reply_id')
    user = request.user  # Assuming you are using Django's built-in authentication system


    if reply_id:
        try:
            parent_reply = Reply.objects.get(id=reply_id)
            reply = Reply.objects.create(
                thread=parent_reply.thread,
                user=user,
                content=request.data['content'],
                parent=parent_reply
            )
        except Reply.DoesNotExist:
            return Response({"error": "Invalid parent reply ID"}, status=400)
        
    # Ensure that the thread_id and content are present in the request
    else:
        if not thread_id or 'content' not in request.data:
            return Response({"error": "Invalid request data"}, status=400)

    # Create the reply object
        reply = Reply.objects.create(
            thread_id=thread_id,
            user=user,
            content=request.data['content'],

        )

    # Serialize the created reply
    serializer = ReplySerializer(reply)

    # Return the serialized data
    return Response(serializer.data, status=201)

@api_view(['GET'])
def reply_list(request):

    queryset = Reply.objects.filter(parent=None).order_by('+created_at')

    search_query= request.query_params.get('search', None)
    thread_query= request.query_params.get('thread', None)

    if search_query:
        queryset= queryset.filter(Q(content__icontains=search_query))
    if thread_query:
        queryset= queryset.filter(Q(thread__exact=thread_query))

    serializer_class= ReplySerializer(queryset, many=True)
    return Response(serializer_class.data)