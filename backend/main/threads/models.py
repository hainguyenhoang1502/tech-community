from django.db import models
from topics.models import Topic
from users.models import CustomUser
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.text import slugify
import re

# Create your models here.
class Thread(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, related_name='threads')
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='threads')
    title = models.CharField(max_length=255)
    slug = models.CharField(max_length=255,blank=True)
    content = RichTextUploadingField()
    created_at = models.DateTimeField(auto_now_add=True)
    publish= models.BooleanField(default=True)

    def __str__(self):
        return self.title
    
    def save_with_id(self, *args, **kwargs):
        if not self.slug:
            self.slug = f"{slugify(self.title)}.{self.pk}"
        if not self.publish:
            self.publish=True
        return super().save(*args, **kwargs)