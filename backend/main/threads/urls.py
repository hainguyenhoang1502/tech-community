from django.urls import path
from . import views

urlpatterns = [
    path('topic/<slug:topic_slug>/', views.list_threads, name='user-list'),
    path('lastest/', views.get_lastest, name='threads-list-lastest'),
    path('create/', views.post_thread, name='thread-create'),
    path('<int:thread_id>/', views.get_detail, name='thread-detail'),
    # Other user-related URLs for the app
]
