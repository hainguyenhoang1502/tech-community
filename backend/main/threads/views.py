from django.shortcuts import render, get_object_or_404
from rest_framework import generics
from .models import Thread
from topics.models import Topic
from catergories.models import Category
from users.models import CustomUser
from .serializers import ThreadSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.db.models import Q
from django.core.paginator import Paginator

@api_view(['GET'])
@permission_classes([AllowAny])
def list_threads(request, topic_slug):
    topic = get_object_or_404(Topic, slug=topic_slug)
    queryset= Thread.objects.filter(Q(topic=topic)).filter(publish__exact=True).order_by("-created_at") 

    search_query= request.query_params.get('s', None)
    topic_query= request.query_params.get('t', None)
    

    if search_query:
        queryset= queryset.filter(Q(title__icontains=search_query))
    if topic_query:
        queryset= queryset.filter(Q(topic__exact=topic_query))

    page=Paginator(queryset, 10)
    page_number=request.GET.get("page")
    page_obj=page.get_page(page_number)

    threads_serializer= ThreadSerializer(page_obj, many=True)
    page_data = {
        'has_next': page_obj.has_next(),
        'has_previous': page_obj.has_previous(),
        'next_page_number': page_obj.next_page_number() if page_obj.has_next() else None,
        'previous_page_number': page_obj.previous_page_number() if page_obj.has_previous() else None,
        'number': page_obj.number,
        'total_pages': page_obj.paginator.num_pages,
        'count': page_obj.paginator.count,
    }

    return Response({"data":threads_serializer.data, "page":page_data})

@api_view(['GET'])
@permission_classes([AllowAny])
def get_detail(request, thread_id):
    try:
        query_set=Thread.objects.filter(id=thread_id)
    except Thread.DoesNotExist:
        return Response({'detail': '404 Not found'}, status=404)
    print(f"query set:{query_set}")
    serializer = ThreadSerializer(query_set, many=True)
    return Response(serializer.data)
    

@api_view(['GET'])
@permission_classes([AllowAny])
def get_lastest(request):
    queryset = Thread.objects.filter(publish__exact=True).order_by('-created_at')[:5]
    serializer = ThreadSerializer(queryset, many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def post_thread(request):
    user = request.user
    topic = request.data.get('topic')
    title = request.data.get('title')
    content = request.data.get('content')
    publish=True

    if not user.has_perm('threads.create_thread'):
        publish=False

    try:
        topic = Topic.objects.get(slug=topic)
    except Topic.DoesNotExist:
        return Response({'detail': 'Invalid topic ID'}, status=400)

    thread = Thread.objects.create(user=user,publish=publish, topic=topic, title=title, content=content)
    thread.save_with_id()
    serializer = ThreadSerializer(thread)
    return Response(serializer.data, status=201)