from rest_framework import serializers
from .models import Thread
from replies.serializers import ReplySerializer
from users.serializers import CustomUserSerializer
class ThreadSerializer(serializers.ModelSerializer):
    user= CustomUserSerializer()
    class Meta:
        model= Thread
        fields='__all__'
class ThreadNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model=Thread
        fields=['id', 'title', 'slug']